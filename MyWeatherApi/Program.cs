﻿using System;
using System.Text.Json;
using System.Web;
using System.Threading.Tasks;
using System.Net.Http;

namespace MyWeatherApi
{
    class Program
    {
        static async Task<int> Main(string[] args)
        {
            string city = Console.ReadLine();
            const string apiKey = "0b17ab8213742be079f8fa1a668cba3c";

            var builderCord = new UriBuilder("https://api.openweathermap.org/data/2.5/weather");
            

            var queryParametersCord = HttpUtility.ParseQueryString(builderCord.Query);
            queryParametersCord["q"] = city;
            queryParametersCord["appid"] = apiKey;
            builderCord.Query = queryParametersCord.ToString();
            Uri uriCord = builderCord.Uri;
            var requestCord = new HttpRequestMessage(HttpMethod.Get, uriCord);
            var client = new HttpClient();
            HttpResponseMessage resultCord = await client.SendAsync(requestCord);
            resultCord.EnsureSuccessStatusCode();

            string jsonContentCord = await resultCord.Content.ReadAsStringAsync();
            JsonDocument jsonDocument = JsonDocument.Parse(jsonContentCord);
            double lat = jsonDocument.RootElement
                .GetProperty("coord")
                .GetProperty("lat")
                .GetDouble();
            double lon = jsonDocument.RootElement
                .GetProperty("coord")
                .GetProperty("lon")
                .GetDouble();

            var builderTemperature = new UriBuilder("https://api.openweathermap.org/data/2.5/onecall");
            var queryParametersTemperature = HttpUtility.ParseQueryString(builderTemperature.Query);
            queryParametersTemperature["lat"] = lat.ToString();
            queryParametersTemperature["lon"] = lon.ToString();
            queryParametersTemperature["exclude"] = "daily";
            queryParametersTemperature["appid"] = apiKey;
            builderTemperature.Query = queryParametersTemperature.ToString();
            Uri uriTemperature = builderTemperature.Uri;
            var requestTemperature = new HttpRequestMessage(HttpMethod.Get, uriTemperature);
            HttpResponseMessage resultTemperature = await client.SendAsync(requestTemperature);
            resultTemperature.EnsureSuccessStatusCode();

            string jsonContentTemperature = await resultTemperature.Content.ReadAsStringAsync();
            JsonDocument jsonDocumentTemperature = JsonDocument.Parse(jsonContentTemperature);
            double kelvinDegrees = jsonDocumentTemperature.RootElement
            .GetProperty("current")
            .GetProperty("temp")
            .GetDouble();

            int dateUnixTimeSeconds = jsonDocumentTemperature.RootElement
            .GetProperty("current")
            .GetProperty("dt")
            .GetInt32();

            DateTimeOffset offsetUtc = DateTimeOffset.FromUnixTimeSeconds(dateUnixTimeSeconds);
            DateTimeOffset offsetLocal = offsetUtc.ToLocalTime();

            Console.WriteLine("Temp in celsius: {0}, date: {1}", KelvinToCelsius(kelvinDegrees), offsetLocal);
            return 0;
        }
        private static string KelvinToCelsius(double kelvinDegrees)
        {
            var celsius = Math.Round(kelvinDegrees - 273.15);
            if (celsius > 0)
                return "+" + celsius.ToString();
            return Math.Round(kelvinDegrees - 273.15).ToString();
        }

    }
}
